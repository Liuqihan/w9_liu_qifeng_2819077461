### 第9周-卷积神经网络分类任务和检测任务
#### 技能掌握
学习图像分类任务目前主要模型算法和检测任务目前主要模型算法。

---
**`学习作业-物理检测模型`**

> 问题描述

利用slim框架和object_detection框架，做一个物体检测的模型。通过这个作业，学员可以了解到物体检测模型的数据准备，训练和验证的过程。

详情见：[https://gitee.com/ai100/quiz-w8-doc](https://gitee.com/ai100/quiz-w8-doc)

> 解题提示

1. 数据集：本次作业使用我们已经准备好的数据集即可，自行制作数据集作为练习项目。

	自行制作数据集：

	- 确定要识别的物体，需要把待识别的物体进行分类，类与类之间要有比较明显的区别特征，比如苹果和香蕉就是两个比较好的分类，而橘子和橙子就不是两个比较好的分类。

	- 采集图片，根据不同的目的，可以采用不同方式对数据进行采集。比如针对一些常见类别，可以直接使用imagenet，coco等已经标定好的数据集，取出需要的分类即可。如果有特定的场景需求，可以针对特定场景需求对图片进行采集，要求要尽量覆盖所有的应用场景。

	- 图片筛选和预处理，需要将图片中不含有待识别物体或者图片很不清晰的去掉。另外相机或者手机拍的图片，尺寸都比较大，建议将图片缩放到1080x720像素或者更低，这样可以避免训练框架运行的时候出现内存不足的问题。
	- 数据标定，使用LableImg工具进行，具体内容阅读一下LabelImg的文档就可以。需要注意数据集的labels文件需要提前准备好，LabelImg有默认的labels设置，不指定的话，在标定的过程中输入labels比较容易出错。

	- 数据整理，将标定好的数据xml和jpg分开存放。推荐按照voc的层级格式存放，图片放到images下面，xml文件放到annotations/xmls下面。

	使用已经准备好的数据集

	这里已经准备好了一个数据集，拥有5个分类，共150张图片，每张图片都做了标注，标注数据格式与voc数据集相同。数据地址如下： 

	[https://gitee.com/ai100/quiz-w8-data.git](https://gitee.com/ai100/quiz-w8-data.git)

	数据集中的物品分类如下：

	- computer
	- monitor
	- scuttlebutt
	- water dispenser
	- drawer chest

	数据集中各目录如下：

	- images， 图片目录，数据集中所有图片都在这个目录下面。
	- annotations/xmls, 针对图片中的物体，使用LabelImg工具标注产生的xml文件都在这里，每个xml文件对应一个图片文件，每个xml文件里面包含图片中多个物体的位置和种类信息。

2. 代码：本次代码使用tensorflow官方代码，代码地址如下：
[https://github.com/tensorflow/models/tree/r1.5](https://github.com/tensorflow/models/tree/r1.5)

	主要使用的是research/object_detection目录下的物体检测框架的代码。这个框架同时引用slim框架的部分内容，需要对运行路径做一下设置，不然会出现找不到module的错误。 设置运行路径的方式有两种：

	1) 直接在代码中插入路径，使用sys.path.insert，具体内容留作学员作业，请自行查找相关资料
	
	2) 使用环境变量PYTHONPATH，参考[https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md)，具体内容留作学员作业，请自行查找相关资料
	本次作业代码中，run.sh已经做好相关的path设置工作，在tinymind上运行可以不用考虑这个问题。

3. 预训练模型：object_detection框架提供了一些预训练的模型以加快模型训练的速度，不同的模型及检测框架的预训练模型不同，常用的模型有resnet，mobilenet以及最近google发布的nasnet，检测框架有faster_rcnn，ssd等，本次作业使用mobilenet模型ssd检测框架，其预训练模型请自行在model_zoo中查找:
[https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md)

> 批改标准

数据准备完成-30分

学员的模型引用的数据集中，应包含以下文件，文件名需要跟此处文件名一致。

- model.ckpt.data-00000-of-00001 预训练模型相关文件
- model.ckpt.index 预训练模型相关文件
- model.ckpt.meta 预训练模型相关文件
- labels_items.txt 数据集中的label_map文件
- pet_train.record 数据准备过程中，从原始数据生成的tfrecord格式的数据
- pet_val.record 数据准备过程中，从原始数据生成的tfrecord格式的数据
- test.jpg 验证图片，取任意一张训练集图片即可

训练过程正常运行并结束-30分

出现以下log，没有明显异常退出的提示，没有明显错误：

	INFO:tensorflow:depth of additional conv before box predictor: 0
	INFO:tensorflow:depth of additional conv before box predictor: 0
	INFO:tensorflow:Summary name /clone_loss is illegal; using clone_loss instead.
	...............
	INFO:tensorflow:Restoring parameters from /path/to/model.ckpt
	INFO:tensorflow:Starting Session.
	INFO:tensorflow:Saving checkpoint to path /path/to/train/model.ckpt
	INFO:tensorflow:Starting Queues.
	INFO:tensorflow:global_step/sec: 0
	INFO:tensorflow:global step 15: loss = 10.6184 (0.212 sec/step)
	INFO:tensorflow:global step 16: loss = 10.7582 (0.221 sec/step)
	INFO:tensorflow:global step 17: loss = 9.4801 (0.209 sec/step)
	INFO:tensorflow:global step 18: loss = 9.4556 (0.219 sec/step)

验证有结果输出-30分

心得体会10分 